//
//  RecordsTableViewCell.swift
//  Submarine
//
//  Created by Veranika Shakh on 06/02/2022.
//

import UIKit

class RecordsTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var roundTimeLabel: UILabel!
    @IBOutlet weak var coinsCountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    // MARK: - Properties
    
    private var labels: [UILabel] {
        [roundTimeLabel, coinsCountLabel, dateLabel, userNameLabel]
    }
        
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()

        labels.forEach { label in
            label.font = UIFont.kopenhagenStyleFont(ofSize: 15)
            label.textColor = .white
        }
    }
    
    // MARK: - Public func
    
    func update(with gameRecord: GameRecord, name: String) {
        self.roundTimeLabel.text = "\(gameRecord.roundTime)"
        self.coinsCountLabel.text = "\(gameRecord.coinsCount)"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm  dd.MM.yyyy"
        let date = formatter.string(from: gameRecord.date)
        self.dateLabel.text = date
        
        self.userNameLabel.text = name
    }
}
