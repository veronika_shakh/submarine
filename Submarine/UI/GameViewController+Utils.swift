//
//  GameViewController+Utils.swift
//  Submarine
//
//  Created by Veranika Shakh on 12/01/2022.
//

import UIKit

extension GameViewController {
    
    func setupSubmarineAppearance() {
        submarineImageView.frame.size = Constant.submarineSize
        submarineImageView.frame.origin.x = submarineImageView.frame.size.width / 2
        submarineImageView.frame.origin.y = view.frame.maxY + submarineImageView.frame.size.height
    }
    
    func setupShipAppearance() {
        shipImageView.frame.size = Constant.shipSize
        shipImageView.frame.origin.y = view.frame.origin.y - (shipImageView.frame.height / 2)
        shipImageView.frame.origin.x = view.frame.maxX
    }
    
    func setupFishAppearance() {
        fishImageView.frame.size = Constant.fishSize
        fishImageView.frame.origin.y = CGFloat.random(in: (view.frame.minY + fishImageView.frame.height)...(view.frame.maxY - fishImageView.frame.height))
        fishImageView.frame.origin.x = view.frame.maxX
    }
    
    func setupCoinAppearance() {
        coinImageView.isHidden = false
        coinImageView.frame.size = Constant.coinSize
        coinImageView.frame.origin.y = CGFloat.random(in: (view.frame.minY + coinImageView.frame.height)...(view.frame.maxY - coinImageView.frame.height))
        coinImageView.frame.origin.x = view.frame.maxX
    }
    
    func setupCrabAppearance() {
        crabImageView.frame.size = Constant.crabSize
        crabImageView.frame.origin.x = view.frame.maxX
        crabImageView.frame.origin.y = view.frame.maxY - crabImageView.frame.height
    }
    
    func setupClockAppearance() {
        clockImageView.isHidden = false
        clockImageView.frame.size = Constant.clockSize
        clockImageView.frame.origin.y = CGFloat.random(in: (view.frame.minY + clockImageView.frame.height)...(view.frame.maxY - clockImageView.frame.height))
        clockImageView.frame.origin.x = view.frame.maxX
    }
    
    func animateShip() {
        UIView.animate(withDuration: Constant.shipAnimationDuration, delay: 0, options: .repeat) {
            self.shipImageView.frame.origin.x = -self.shipImageView.frame.width - self.containerView.frame.origin.x
        } 
    }
    
    func animateFish() {
        UIView.animate(withDuration: Constant.fishAnimationDuration, delay: 1) {
            self.fishImageView.frame.origin.x = -self.fishImageView.frame.width - self.containerView.frame.origin.x
        } completion: { isFinished in
            if isFinished == true {
                self.setupFishAppearance()
                self.animateFish()
            }
        }
    }
    
    func animateCoin() {
        UIView.animate(withDuration: Constant.coinAnimationDuration, delay: 2) {
            self.coinImageView.frame.origin.x = -self.coinImageView.frame.width - self.containerView.frame.origin.x
        } completion: { isFinished in
            if isFinished == true {
                self.setupCoinAppearance()
                self.animateCoin()
            }
        }
    }
    
    func animateCrab() {
        UIView.animateKeyframes(withDuration: Constant.crabAnimationDuration, delay: 2.5, options: .repeat) {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.25) {
                self.crabImageView.frame.origin.x = self.view.frame.maxX - (self.view.frame.maxX / 1.5)
            }
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.25) {
                self.crabImageView.frame.origin.x = self.crabImageView.frame.origin.x - (self.crabImageView.frame.width * 2)
                self.crabImageView.frame.origin.y = self.crabImageView.frame.origin.y - (self.crabImageView.frame.height * 2)
            }
            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.25) {
                self.crabImageView.frame.origin.x = self.crabImageView.frame.origin.x - (self.crabImageView.frame.width * 2)
                self.crabImageView.frame.origin.y = self.crabImageView.frame.origin.y + (self.crabImageView.frame.height * 2)
            }
            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.25) {
                self.crabImageView.frame.origin.x = self.crabImageView.frame.origin.x - (self.crabImageView.frame.width * 2)
                self.crabImageView.frame.origin.y = self.crabImageView.frame.origin.y - (self.crabImageView.frame.height * 2)
            }
            UIView.addKeyframe(withRelativeStartTime: 0.8, relativeDuration: 0.25) {
                self.crabImageView.frame.origin.x = self.crabImageView.frame.origin.x - (self.crabImageView.frame.width * 3)
                self.crabImageView.frame.origin.y = self.crabImageView.frame.origin.y + (self.crabImageView.frame.height * 3)
            }
        } 
    }
    
    func animateClock() {
        UIView.animate(withDuration: Constant.clockAnimationDuration, delay: 3) {
            self.clockImageView.frame.origin.x = -self.clockImageView.frame.width - self.clockImageView.frame.origin.x
        } completion: { isFinished in
            if isFinished == true {
                self.setupClockAppearance()
                self.animateClock()
            }
        }
    }
}
