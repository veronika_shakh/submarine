//
//  SettingsViewController.swift
//  Submarine
//
//  Created by Veronika Shakh on 11/25/21.
//

import UIKit

class SettingsViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var submarineLabel: UILabel!
    @IBOutlet weak var enemyLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var darkSubmarineButton: UIButton!
    @IBOutlet weak var orangSubmarineButton: UIButton!
    @IBOutlet weak var nemoFishButton: UIButton!
    @IBOutlet weak var leonFishButton: UIButton!
    
    // MARK: - Properties
    
    private var submarineImageName: String?
    private var fishImageName: String?
    
    private var buttons: [UIButton] {
        [saveButton, backButton, darkSubmarineButton, orangSubmarineButton, nemoFishButton, leonFishButton]
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.delegate = self
        
        buttons.forEach { button in
            button.roundCorners(radius: 15, borderWidth: 4, borderColor: .white)
            button.addShadow()
            button.titleLabel?.font = UIFont.kopenhagenStyleFont(ofSize: 50)
        }
        
        [nameLabel, submarineLabel, enemyLabel].forEach { label in
            guard let label = label else { return }
            label.roundCorners(borderWidth: 4.0,
                               borderColor: .white)
            label.font = UIFont.kopenhagenStyleFont(ofSize: 25)
        }
        
        let settings = SettingsManager.shared.getSettings()
        selectSubmarine(name: settings.submarineImageName)
        selectEnemy(name: settings.fishImageName)
        nameTextField.text = settings.userName
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        buttons.forEach { button in
            button.titleLabel?.font = UIFont.kopenhagenStyleFont(ofSize: 40)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let nameTitle = "Name".locacalized
        self.nameLabel.text = nameTitle
        
        let submarineTitle = "Submarine".locacalized
        self.submarineLabel.text = submarineTitle
        
        let enemyTitle = "Enemy".locacalized
        self.enemyLabel.text = enemyTitle
        
        let saveTitle = "Save".locacalized
        self.saveButton.setTitle(saveTitle, for: .normal)
        
        let backTitle = "Back".locacalized
        self.backButton.setTitle(backTitle, for: .normal)
    }
    
    // MARK: - Action
    
    @IBAction func saveButtonDidTap(_ sender: UIButton) {
        SettingsManager.shared.save(userName: nameTextField.text,
                                    submarineImageName: self.submarineImageName,
                                    fishImageName: self.fishImageName)
        
        saveButton.addShadow(color: UIColor.blue)
        saveButton.roundCorners(borderColor: .blue)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backButtonDidTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func darkSubmarineDidTap(_ sender: UIButton) {
        submarineImageName = Settings.Constant.customSubmarine
        selectSubmarine(name: Settings.Constant.customSubmarine)
    }
    
    @IBAction func orangSudmarineDidTap(_ sender: UIButton) {
        submarineImageName = Settings.Constant.defaultSubmarine
        selectSubmarine(name: Settings.Constant.defaultSubmarine)
    }
    
    @IBAction func nemoFishDidTap(_ sender: UIButton) {
        fishImageName = Settings.Constant.defaultFish
        selectEnemy(name: Settings.Constant.defaultFish)
    }
    
    @IBAction func leonFishDidTap(_ sender: UIButton) {
        fishImageName = Settings.Constant.customFish
        selectEnemy(name: Settings.Constant.customFish)
    }
    
    // MARK: - Private func
    
    private func selectSubmarine(name: String) {
        if name == Settings.Constant.defaultSubmarine {
            orangSubmarineButton.addShadow(color: .blue)
            orangSubmarineButton.roundCorners(borderColor: .blue)
            darkSubmarineButton.addShadow(color: .white)
            darkSubmarineButton.roundCorners(borderColor: .white)
        } else {
            darkSubmarineButton.addShadow(color: .blue)
            darkSubmarineButton.roundCorners(borderColor: .blue)
            orangSubmarineButton.addShadow(color: .white)
            orangSubmarineButton.roundCorners(borderColor: .white)
        }
    }
    
    private func selectEnemy(name: String) {
        if name == Settings.Constant.defaultFish {
            nemoFishButton.addShadow(color: .blue)
            nemoFishButton.roundCorners(borderColor: .blue)
            leonFishButton.addShadow(color: .white)
            leonFishButton.roundCorners(borderColor: .white)
        } else {
            leonFishButton.addShadow(color: .blue)
            leonFishButton.roundCorners(borderColor: .blue)
            nemoFishButton.addShadow(color: .white)
            nemoFishButton.roundCorners(borderColor: .white)
        }
    }
}

extension SettingsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
