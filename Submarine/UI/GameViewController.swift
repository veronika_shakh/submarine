//
//  GameViewController.swift
//  Submarine
//
//  Created by Veronika Shakh on 11/25/21.
//

import UIKit
import CoreMotion

class GameViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var submarineImageView: UIImageView!
    @IBOutlet weak var shipImageView: UIImageView!
    @IBOutlet weak var fishImageView: UIImageView!
    @IBOutlet weak var crabImageView: UIImageView!
    @IBOutlet weak var coinImageView: UIImageView!
    @IBOutlet weak var clockImageView: UIImageView!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var coinsLabel: UILabel!
    
    @IBOutlet weak var playPauseButton: UIButton!
    
    @IBOutlet weak var backMenuConstraint: NSLayoutConstraint!
    @IBOutlet weak var recognizerView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    
    // MARK: - Properties
    
    private var gameTimer: Timer?
    private var coalinsionsTimer: Timer?
    
    private var timeLeft = Constant.roundTime
    private var coinsCount = Constant.startCoinCaunt
    
    private var accelerometerManager = CMMotionManager()
    
    private var gameStartTime: Date?
    private var raundDuration = 0
    
    private var gameState: GameState = .start
    
    private enum GameState {
        case start
        case pause
        case play
    }
    
    private var enemies: [UIView] {
        [shipImageView, fishImageView, crabImageView]
    }
    
    enum Constant {
        static let submarineSize = CGSize(width: 100, height: 60)
        static let submarineAnimationDuration: TimeInterval = 0.3
        static let shipSize = CGSize(width: 120, height: 120)
        static let shipAnimationDuration: TimeInterval = 3
        static let fishSize = CGSize(width: 40, height: 40)
        static let fishAnimationDuration: TimeInterval = 3
        static let coinSize = CGSize(width: 30, height: 30)
        static let coinAnimationDuration: TimeInterval = 3
        static let roundTime = 60
        static let startCoinCaunt = 0
        static let crabSize = CGSize(width: 30, height: 30)
        static let crabAnimationDuration: TimeInterval = 4
        static let clockSize = CGSize(width: 30, height: 30)
        static let clockAnimationDuration: TimeInterval = 4
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.isHidden = true
        userNameLabel.isHidden = true
        playPauseButton.isHidden = true
        
        let settings = SettingsManager.shared.getSettings()
        submarineImageView.image = UIImage(named: settings.submarineImageName)
        fishImageView.image = UIImage(named: settings.fishImageName)
        
        let userName = settings.userName
        userNameLabel.text = userName
        userNameLabel.attributedText = createAttributedString(string: userName)
        
        addRecognizer()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupShipAppearance()
        setupSubmarineAppearance()
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        super.motionEnded(motion, with: event)
        
        switch motion {
        case .motionShake:
            submarineSurface()

        default: break
        }
    }
    
    // MARK: - Action
    
    @IBAction func repeatButtonDidTap(_ sender: UIButton) {
        startGame()
    }
    
    @IBAction func backButtonDidTap(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func playPauseButtonDidTap(_ sender: UIButton) {
        switch gameState {
        case .play:
            gameState = .pause
            pauseGame()
            playPauseButton.setImage(UIImage(named: "play"), for: .normal)
            
        case .pause:
            gameState = .play
            resumeGame()
            playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
            
        case .start: break
        }
    }
    
    @IBAction func swipeDownDetected(_recognizer: UITapGestureRecognizer) {
        showBackButton()
    }
    
    @IBAction func swipeUpDetected(_recognizer: UITapGestureRecognizer) {
        hideBackBatton()
    }
    
    // MARK: - Private func
    
    // MARK: - Collision
    
    private func checkSubmarineCollision() {
        for enemy in enemies {
            if let enemyPresentation = enemy.layer.presentation(),
               let submarinePresentation = submarineImageView.layer.presentation(),
               enemyPresentation.frame.intersects(submarinePresentation.frame) {
                stopGame()
                return
            }
        }
    }
    
    private func checkFriendsCollision() {
        if let coinPresentation = coinImageView.layer.presentation(),
           let submarinePresentation = submarineImageView.layer.presentation(),
           coinPresentation.frame.intersects(submarinePresentation.frame) {
            addCoins()
        }
        
        if let clockPresentation = clockImageView.layer.presentation(),
           let submarinePresentation = submarineImageView.layer.presentation(),
           clockPresentation.frame.intersects(submarinePresentation.frame) {
            addTime()
        }
    }
    
    // MARK: - Game Lifecycle
    
    private func submarineSurface() {
        guard gameState == .start else { return }
        
        gameState = .play
        
        UIView.animate(withDuration: 0.3) {
            self.submarineImageView.frame.origin.y = (self.view.frame.height / 2) - (self.submarineImageView.frame.size.height / 2)
        } completion: { _ in
            self.startGame()
        }
    }
    
    private func prepareGameScene() {
        containerView.isHidden = true
        userNameLabel.isHidden = false
        playPauseButton.isHidden = false
        playPauseButton.setImage(UIImage(named: "pause"), for: .normal)
    }
    
    private func startGame() {
        prepareGameScene()
    
        setupShipAppearance()
        setupCoinAppearance()
        setupCrabAppearance()
        setupClockAppearance()
        animateShip()
        animateFish()
        animateCoin()
        animateCrab()
        animateClock()
        startAccelerometer()
        startGameTimer()
        startCoinsCaunting()
        hideBackBatton()
        
        coalinsionsTimer = Timer.scheduledTimer(timeInterval: 0.3,
                                                target: self,
                                                selector: #selector(onCoinsTimerFire),
                                                userInfo: nil,
                                                repeats: true)
        
        gameStartTime = Date()
    }
    
    private func pauseGame() {
        shipImageView.pauseAnimation()
        submarineImageView.pauseAnimation()
        fishImageView.pauseAnimation()
        coinImageView.pauseAnimation()
        crabImageView.pauseAnimation()
        clockImageView.pauseAnimation()
        
        gameTimer?.invalidate()
        accelerometerManager.stopAccelerometerUpdates()
        
        let gamePauseDate = Date()
        let gameStartDate = gameStartTime ?? Date()
        let lastGamePeriodDuration = Int(gamePauseDate.timeIntervalSince(gameStartDate))
        raundDuration += lastGamePeriodDuration
    }
    
    private func resumeGame() {
        startAccelerometer()
        self.gameTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                          target: self,
                                          selector: #selector(onGameTimerFire),
                                          userInfo: nil,
                                          repeats: true)
        
        self.gameStartTime = Date()
        
        shipImageView.resumeAnimation()
        submarineImageView.resumeAnimation()
        fishImageView.resumeAnimation()
        coinImageView.resumeAnimation()
        crabImageView.resumeAnimation()
        clockImageView.resumeAnimation()
    }
    
    private func stopGame() {
        shipImageView.stopAnimation()
        fishImageView.stopAnimation()
        coinImageView.stopAnimation()
        crabImageView.stopAnimation()
        clockImageView.stopAnimation()
        view.stopAnimation()
        view.layoutIfNeeded()
        
        accelerometerManager.stopAccelerometerUpdates()
    
        timerLabel.text = ""
        coinsLabel.text = ""
        
        containerView.isHidden = false
        userNameLabel.isHidden = true
        playPauseButton.isHidden = true
        
        showBackButton()
        saveGameRecord()
        
        coalinsionsTimer?.invalidate()
        coalinsionsTimer = nil
        gameTimer?.invalidate()
        gameTimer = nil
    }
    
    private func saveGameRecord() {
        let coinsCount = self.coinsCount
        let gameFinishTime = Date()
        let gameStartDate = gameStartTime ?? Date()
        let roundTime = Int(gameFinishTime.timeIntervalSince(gameStartDate)) + self.raundDuration
        
        let gameRecord = GameRecord(roundTime: roundTime,
                                    coinsCount: coinsCount,
                                    date: gameStartDate)
        
        RecordsManager.shared.saveGameResult(gameResult: gameRecord)
    }
    
    private func startAccelerometer() {
        if accelerometerManager.isAccelerometerAvailable {
            accelerometerManager.accelerometerUpdateInterval = 0.1
            accelerometerManager.startAccelerometerUpdates(to: .main) { [weak self] data, error in
                guard
                    let self = self,
                    let accelerometer = data?.acceleration
                else { return }
                
                UIView.animate(withDuration: Constant.submarineAnimationDuration) {
                    let targetY = accelerometer.x * self.view.frame.height / 2.0 + self.view.frame.height / 2.0 - self.submarineImageView.frame.height / 2.0
                    self.submarineImageView.frame.origin.y = targetY
                }
            }
        }
    }
    
    // MARK: - Game Timer
    
    private func startGameTimer() {
        timeLeft = Constant.roundTime
        updateTimeLabel()
        
        gameTimer?.invalidate()
        gameTimer = Timer.scheduledTimer(timeInterval: 1.0,
                                          target: self,
                                          selector: #selector(onGameTimerFire),
                                          userInfo: nil,
                                          repeats: true)
    }
    
    @objc private func onGameTimerFire() {
        if timeLeft > 0 {
            timeLeft -= 1
            updateTimeLabel()
        }
        
        if timeLeft <= 0 {
            gameTimer?.invalidate()
            gameTimer = nil
            stopGame()
        }
    }
    
    // MARK: - Coins
    
    private func startCoinsCaunting() {
        coinsCount = Constant.startCoinCaunt
        updateCoinsLabel()
    }
    
    @objc private func onCoinsTimerFire() {
        checkSubmarineCollision()
        checkFriendsCollision()
    }
    
    private func addCoins() {
        coinsCount += 5
        updateCoinsLabel()
        coinImageView.isHidden = true
    }
    
    private func updateCoinsLabel() {
        let money = "Money".locacalized
        coinsLabel.attributedText = createAttributedString(string: "\(money) \n\(coinsCount)")
    }
    
    // MARK: - Time
    
    private func addTime() {
        timeLeft += 15
        updateTimeLabel()
        clockImageView.isHidden = true
    }
    
    private func updateTimeLabel() {
        let time = "Time".locacalized
        timerLabel.attributedText = createAttributedString(string: "\(time) \n\(timeLeft)")
    }
    
    // MARK: - Back button
    
    private func showBackButton() {
        backMenuConstraint.constant = 20
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func hideBackBatton() {
        backMenuConstraint.constant = -50
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func addRecognizer() {
        let downSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeDownDetected))
        downSwipeRecognizer.direction = .down
        self.recognizerView.addGestureRecognizer(downSwipeRecognizer)
        
        let upSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeUpDetected))
        upSwipeRecognizer.direction = .up
        self.recognizerView.addGestureRecognizer(upSwipeRecognizer)
    }
}

// MARK: - GameViewController + GameViewController

extension GameViewController {
    
    func createAttributedString(string: String) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let attributes:[NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.sRed,
            .font: UIFont.kopenhagenStyleFont(ofSize: 20),
            .paragraphStyle: paragraphStyle
        ]
        
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
}
