//
//  StartViewController.swift
//  Submarine
//
//  Created by Veronika Shakh on 11/25/21.
//

import UIKit

class StartViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var gameButton: UIButton!
    @IBOutlet weak var recordsButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    // MARK: - Properties
    
    private var buttons: [UIButton] {
        [gameButton, recordsButton, settingsButton]
    }
    
    // MARK: - Lifecycle
  
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        buttons.forEach { button in
            button.roundCorners(radius: 15, borderWidth: 4, borderColor: .white)
            button.addShadow()
            button.titleLabel?.font = UIFont.kopenhagenStyleFont(ofSize: 48)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let gameTitle = "Game".locacalized
        self.gameButton.setTitle(gameTitle, for: .normal)
        
        let recordsTitle = "Records".locacalized
        self.recordsButton.setTitle(recordsTitle, for: .normal)
        
        let settingstitle = "Settings".locacalized
        self.settingsButton.setTitle(settingstitle, for: .normal)
    }
    
    // MARK: - Action
    
    @IBAction func gameButtonDidTap(_ sender: UIButton) {
        if let gameVC = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController {
            self.present(gameVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func recordsButtonDidTap(_ sender: UIButton) {
        if let recordsVC = self.storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as? RecordsViewController {
            self.present(recordsVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func settingsButtonDidTap(_ sender: UIButton) {
        if let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController {
            self.present(settingsVC, animated: true, completion: nil)
        }
    }
}
