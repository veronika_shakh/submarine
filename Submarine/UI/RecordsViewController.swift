//
//  RecordsViewController.swift
//  Submarine
//
//  Created by Veronika Shakh on 11/25/21.
//

import UIKit

class RecordsViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var recordsTableView: UITableView!
    @IBOutlet weak var timeRoundLabel: UILabel!
    @IBOutlet weak var coinsCountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: - Properties
    
    private var labels: [UILabel] {
        [timeRoundLabel, coinsCountLabel, dateLabel, userNameLabel]
    }
    
    private var records: [GameRecord] = []
    private var settings = SettingsManager.shared.getSettings()
    
    private let fontSize = 20.0
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gameRecords = RecordsManager.shared.getGameResults()
        self.records = gameRecords.sorted(by: { game1, game2 in
            return game1.date.compare(game2.date) == .orderedDescending
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let roundTimeTitle = "Time round".locacalized
        self.timeRoundLabel.text = roundTimeTitle
        
        let coinsCountTitle = "Count coins".locacalized
        self.coinsCountLabel.text = coinsCountTitle
        
        let dateTitle = "Date".locacalized
        self.dateLabel.text = dateTitle
        
        let userNameTitle = "Name".locacalized
        self.userNameLabel.text = userNameTitle
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        labels.forEach { label in
            label.font = UIFont.kopenhagenStyleFont(ofSize: self.fontSize)
            label.textColor = .white
        }
        
        backButton.titleLabel?.font = UIFont.kopenhagenStyleFont(ofSize: self.fontSize)
        backButton.roundCorners(radius: backButton.frame.width / 2,
                                borderWidth: 2,
                                borderColor: .white)
    }
    
    // MARK: - Action
    
    @IBAction func backButtonDidTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource

extension RecordsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecordsTableViewCell",
                                                     for: indexPath) as? RecordsTableViewCell
        else {
            return UITableViewCell()
        }
        
        let record = records[indexPath.row]
        cell.update(with: record, name: self.settings.userName)
        
        return cell
    }
}
