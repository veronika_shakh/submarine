//
//  UIFont+Utils.swift
//  Submarine
//
//  Created by Veranika Shakh on 29/12/2021.
//

import UIKit

extension UIFont {
    static func kopenhagenStyleFont(ofSize fontSize: CGFloat) -> UIFont {
        UIFont(name: "Kopenhagen", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
}
