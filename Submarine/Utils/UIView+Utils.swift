//
//  UIView+Utils.swift
//  Submarine
//
//  Created by Veranika Shakh on 22/12/2021.
//

import UIKit

extension UIView {
    
    func roundCorners(radius: CGFloat = 15,
                      borderWidth: CGFloat = 4.0,
                      borderColor: UIColor? = nil) {
        
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor?.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    func addShadow(color: UIColor = UIColor.white,
                   offset: CGSize = CGSize(width: 5, height: 15),
                   radius: CGFloat = 10) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shouldRasterize = true
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    }
}

extension UIView {
    
    func pauseAnimation() {
        let pausedTime: CFTimeInterval = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
    }

    func resumeAnimation() {
        let pausedTime: CFTimeInterval = layer.timeOffset
        layer.speed = 1.0
        layer.timeOffset = 0.0
        layer.beginTime = 0.0
        let timeSincePause: CFTimeInterval = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        layer.beginTime = timeSincePause
    }
    
    func stopAnimation() {
        layer.removeAllAnimations()
    }
    
}
