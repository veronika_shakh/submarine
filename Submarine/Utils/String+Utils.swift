//
//  String+Utils.swift
//  Submarine
//
//  Created by Veranika Shakh on 06/03/2022.
//

import Foundation

extension String {
    var locacalized: String {
        return NSLocalizedString(self, comment: "")
    }
}
