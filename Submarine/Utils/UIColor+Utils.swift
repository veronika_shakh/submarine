//
//  UIColor+Utils.swift
//  Submarine
//
//  Created by Veranika Shakh on 25/05/2022.
//

import Foundation
import UIKit

extension UIColor {
    static let sRed = UIColor(named: "subRed") ?? .red
}
