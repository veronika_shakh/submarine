//
//  SettingsManager.swift
//  Submarine
//
//  Created by Veranika Shakh on 19/01/2022.
//

import Foundation

class SettingsManager {
    
    static let shared = SettingsManager()
    private init() {}
    
    let settingsKey = "settings"
    
    func save(userName: String?, submarineImageName: String?, fishImageName: String?) {
        let startSettings = getSettings()
        let userName = userName ?? startSettings.userName
        let submarineImageName = submarineImageName ?? startSettings.submarineImageName
        let fishImageName = fishImageName ?? startSettings.fishImageName
        
        let settings = Settings(userName: userName,
                                submarineImageName: submarineImageName,
                                fishImageName: fishImageName)
        UserDefaults.standard.set(encodable: settings, forKey: settingsKey)
    }
    
    func getSettings() -> Settings {
        let settings = UserDefaults.standard.value(Settings.self, forKey: settingsKey)
        if let settings = settings {
            return settings
        }
        
        return Settings.defaultSettings()
    }
}
