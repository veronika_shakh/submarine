//
//  Settings.swift
//  Submarine
//
//  Created by Veranika Shakh on 13/01/2022.
//

import Foundation

class Settings: Codable {
    
    var userName: String
    var submarineImageName: String
    var fishImageName: String
    
    enum Constant {
        static let defaultSubmarine = "Submarine"
        static let customSubmarine = "submarineDark"
        static let defaultFish = "nemoFish"
        static let customFish = "lionFish"
    }
    
    init(userName: String, submarineImageName: String,fishImageName: String) {
        self.userName = userName
        self.submarineImageName = submarineImageName
        self.fishImageName = fishImageName
    }
        
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(userName, forKey: .userName)
        try container.encode(submarineImageName, forKey: .submarineImageName)
        try container.encode(fishImageName, forKey: .fishImageName)
    }
        
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.userName = try container.decode(String.self, forKey: .userName)
        self.submarineImageName = try container.decode(String.self, forKey: .submarineImageName)
        self.fishImageName = try container.decode(String.self, forKey: .fishImageName)
    }
    
    private enum CodingKeys: String, CodingKey {
        case userName
        case submarineImageName
        case fishImageName
    }
    
    
}

extension Settings {
     static func defaultSettings() -> Settings {
         return Settings(userName: "",
                         submarineImageName: Constant.defaultSubmarine,
                         fishImageName: Constant.defaultFish)
     }
}
