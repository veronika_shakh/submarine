//
//  GameRecord.swift
//  Submarine
//
//  Created by Veranika Shakh on 15/01/2022.
//

import Foundation

class GameRecord: Codable {
    
    var roundTime: Int
    var coinsCount: Int
    var date: Date
 
    init(roundTime: Int, coinsCount: Int, date: Date) {
        self.roundTime = roundTime
        self.coinsCount = coinsCount
        self.date = date
    }
        
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(roundTime, forKey: .roundTime)
        try container.encode(coinsCount, forKey: .coinsCount)
        try container.encode(date, forKey: .date)
    }
        
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.roundTime = try container.decode(Int.self, forKey: .roundTime)
        self.coinsCount = try container.decode(Int.self, forKey: .coinsCount)
        self.date = try container.decode(Date.self, forKey: .date)
    }
    
    private enum CodingKeys: String, CodingKey {
        case roundTime
        case coinsCount
        case date
    }
}
