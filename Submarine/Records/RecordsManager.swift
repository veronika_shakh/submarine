//
//  RecordsManager.swift
//  Submarine
//
//  Created by Veranika Shakh on 17/01/2022.
//

import Foundation

class RecordsManager {
    
    static let shared = RecordsManager()
    private init() {}
    
    let keyGameRecords = "gameRecords"
    
    func getGameResults() -> [GameRecord] {
        let allGameRecords = UserDefaults.standard.value([GameRecord].self, forKey: keyGameRecords)
        return allGameRecords ?? []
    }
   
    func saveGameResult(gameResult: GameRecord) {
        var allGameRecords = getGameResults()
        allGameRecords.append(gameResult)
        UserDefaults.standard.set(encodable: allGameRecords, forKey: keyGameRecords)
    }
}
